﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class CountDown : MonoBehaviour
{
   public delegate void TimeStopEventHandler(object sender, EventArgs e);
   public event TimeStopEventHandler TimeStopEvent;

   protected virtual void RaiseTimeStopEvent()
   {
      if (TimeStopEvent != null)
         TimeStopEvent(this, new EventArgs());
   }

   public int numSeconds = 30;
   int timeLeft = 0;
   Text timeText; //UI Text Object
   bool isStarted = false;

   void Start()
   {
      timeText = GetComponent<Text>();
      UpdateTime();
   }

   void Update()
   {
      if (isStarted)
      {
         UpdateTime();
      }
   }

   public void UpdateTime()
   {
      timeText.text = timeLeft.ToString();
   }

   public void StartTimer()
   {
      gameObject.SetActive(true);
      isStarted = true;
      timeLeft = numSeconds;
      StartCoroutine("ReduceTime");
      Time.timeScale = 1; //Just making sure that the timeScale is right
   }

   //Simple Coroutine
   IEnumerator ReduceTime()
   {
      while (true)
      {
         yield return new WaitForSeconds(1);
         timeLeft--;

         if (timeLeft <= 0)
         {
            gameObject.SetActive(false);
            RaiseTimeStopEvent();
            isStarted = false;
         }
      }
   }
}
