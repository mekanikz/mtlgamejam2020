﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class GameManager : MonoBehaviour
{
   public static float defaultThrust = 1.0f;
   CountDown timer;
   GameObject dialoguePanel;
   PanelMover panelMover;
   Flowchart intro;
   Flowchart outro;
   Flowchart positiveBarks;
   Flowchart negativeBarks;

   int museumPieceIndex = 0;

   AudioSource statueBreakSound;
   AudioSource applauseSound;
   AudioSource curtainSound;
   AudioSource gameTrack1;
   AudioSource gameTrack2;

   public List<GameObject> breakables;

   public GameObject userInterface;
   public GameObject scoreWidget;

   public GameObject curtains;

   private void Awake() {
      userInterface.SetActive(true);
      curtains.SetActive(true);
      scoreWidget.SetActive(false);
      museumPieceIndex = 0;
   }

   public void Start()
   {
      GameObject timerObject = GameObject.Find("Timer");
      timerObject.SetActive(false);
      timer = timerObject.GetComponent<CountDown>();
      timer.TimeStopEvent += TimeStopEvent;
      dialoguePanel = GameObject.Find("DialoguePanel");
      dialoguePanel.SetActive(true);
      intro = GameObject.Find("Intro").GetComponent<Flowchart>();
      outro = GameObject.Find("Outro").GetComponent<Flowchart>();
      positiveBarks = GameObject.Find("PositiveBarks").GetComponent<Flowchart>();
      negativeBarks = GameObject.Find("NegativeBarks").GetComponent<Flowchart>();
      statueBreakSound = GameObject.Find("StatueBreakSound").GetComponent<AudioSource>();
      applauseSound = GameObject.Find("ApplauseSound").GetComponent<AudioSource>();
      curtainSound = GameObject.Find("CurtainSound").GetComponent<AudioSource>();
      gameTrack1 = GameObject.Find("GameTrack1").GetComponent<AudioSource>();
      gameTrack2 = GameObject.Find("GameTrack2").GetComponent<AudioSource>();

      gameTrack1.Play();

      DisableMuseumPieces();
   }

   void DisableMuseumPieces()
   {
      foreach (GameObject breakable in breakables)
      {
         breakable.gameObject.SetActive(false);
      }
   }

   public void OnStartGame()
   {
      Debug.Log("Start game");
      gameTrack1.Stop();
      statueBreakSound.Play();
      
      breakables[museumPieceIndex].SetActive(true);
      breakables[museumPieceIndex].gameObject.GetComponent<PieceManager>().BreakApart();

      dialoguePanel.SetActive(false);
      curtainSound.Play();
      curtains.gameObject.GetComponent<PanelMover>().Open();

      StartCoroutine(StartGameAfterBreak());
   }

   IEnumerator StartGameAfterBreak()
   {
      yield return new WaitForSeconds(1.0f);
      timer.StartTimer();
      scoreWidget.SetActive(false);
      gameTrack2.Play();
   }

   public void OnEndGame()
   {
      IncrementMuseumPieceIndex();
      DisableMuseumPieces();
      curtains.gameObject.GetComponent<PanelMover>().Close();
      intro.ExecuteBlock("Intro");
   }

   void IncrementMuseumPieceIndex()
   {
      museumPieceIndex++;
      museumPieceIndex %= (breakables.Count);
   }

   void TimeStopEvent(object sender, EventArgs e)
   {
      Debug.Log("Game end");
      gameTrack2.Stop();
      gameTrack1.Play();

      Score score = breakables[museumPieceIndex].GetComponent<PieceManager>().GetScore();
      scoreWidget.GetComponent<ScoreWidget>().UpdateScoreText(score);
      breakables[museumPieceIndex].GetComponent<PieceManager>().ResetPieces();

      scoreWidget.SetActive(true);
      dialoguePanel.SetActive(true);


      if (score.placedPieceCount >= score.pieceCount)
      {
         applauseSound.Play();
         positiveBarks.ExecuteBlock("PositiveBarks");
      }
      else
      {
         negativeBarks.ExecuteBlock("NegativeBarks");
      }
   }
}
