﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Fungus;

public class MainMenu : MonoBehaviour
{
   Flowchart mainDialogue;
   GameObject mainMenuPanel;

   public void Start()
   {
      mainDialogue = GameObject.Find("MainDialogue").GetComponent<Flowchart>();
      mainMenuPanel = GameObject.Find("MainMenuPanel");
   }

   public void OnPlay()
   {
      mainDialogue.ExecuteBlock("MainMenu");
      mainMenuPanel.SetActive(false);
   }
}
