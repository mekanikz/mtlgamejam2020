﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePart : MonoBehaviour
{
   public float thrust = 100f;

   Rigidbody rb;
   SelectPart selectPart;

   // Start is called before the first frame update
   void Start()
   {
      rb = GetComponent<Rigidbody>();
      selectPart = GetComponent<SelectPart>();

      StopPart();
   }

   void FixedUpdate()
   {
      if (selectPart.IsSelected)
      {
         if (Input.GetKey(KeyCode.W))
         {
            AddForce(Vector3.up);
         }
         else if (Input.GetKey(KeyCode.A))
         {
            AddForce(-Vector3.right);
         }
         else if (Input.GetKey(KeyCode.S))
         {
            AddForce(-Vector3.up);
         }
         else if (Input.GetKey(KeyCode.D))
         {
            AddForce(Vector3.right);
         }
      }
   }

   void OnCollisionEnter(Collision collision)
   {
      rb.velocity = Vector3.zero;
      rb.angularVelocity = Vector3.zero;
      rb.constraints = RigidbodyConstraints.FreezeAll;
   }

   void AddForce(Vector3 direction)
   {
      rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
      rb.WakeUp();
      rb.AddForce(direction * thrust);
   }

   public void StopPart()
   {
      // Stop the part.
      rb.velocity = Vector3.zero;
      rb.angularVelocity = Vector3.zero;
      rb.constraints = RigidbodyConstraints.FreezeAll;
      rb.Sleep();
   }
}
