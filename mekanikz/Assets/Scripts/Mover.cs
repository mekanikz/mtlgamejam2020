﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
     public float speed = 10f;
     private Vector3 targetPos;
     private bool isMoving;
     // Use this for initialization1
     void Start () {
 
         targetPos = transform.position;
         isMoving = false;
     }
     
     // Update is called once per frame
     void Update () {
         if(isMoving)
            MoveObject();
     }

      void OnMouseDown() {
         this.GetComponent<Rigidbody>().useGravity = false;
         this.GetComponent<Rigidbody>().isKinematic = false;
      }

      void OnMouseDrag() {
         isMoving = true;
         SetTargetPosition();
      }

      void OnMouseUp() {
         this.GetComponent<Rigidbody>().isKinematic = true;
      }

     void SetTargetPosition()
     {
         Plane plane = new Plane(Vector3.forward,transform.position);
         Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
         float point = 0f;
 
         if(plane.Raycast(ray, out point)) {
             targetPos = ray.GetPoint(point);
             targetPos.z = this.transform.position.z;
         }
         
         isMoving = true;
     }
     void MoveObject()
     {
//         transform.LookAt(targetPos);
         transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
 
         if (transform.position == targetPos)
             isMoving = false;
//         Debug.DrawLine(transform.position,targetPos,Color.red);
 
     }
 }
/*
    public float thrust = 0.0f;

    private Rigidbody rb;

    private PositionTracker positionTracker;

    private bool isSelected;

   public void Start() {
      isSelected = false;
      positionTracker = transform.parent.GetComponent<PositionTracker>();
      rb = this.GetComponent<Rigidbody>();
      thrust = (thrust == 0.0f ? GameManager.defaultThrust : thrust);

      StopPart();
   }

   void FixedUpdate()
   {
      if (Input.GetKey(KeyCode.W))
      {
         AddForce(Vector3.up);
      }
      else if (Input.GetKey(KeyCode.A))
      {
         AddForce(-Vector3.right);
      }
      else if (Input.GetKey(KeyCode.S))
      {
         AddForce(-Vector3.up);
      }
      else if (Input.GetKey(KeyCode.D))
      {
         AddForce(Vector3.right);
      }
   }

   void OnCollisionEnter(Collision collision)
   {
      rb.velocity = Vector3.zero;
      rb.angularVelocity = Vector3.zero;
      rb.constraints = RigidbodyConstraints.FreezeAll;
   }

   void AddForce(Vector3 direction)
   {
      rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
      rb.WakeUp();
      rb.AddForce(direction * thrust);
   }

   void StopPart()
   {
      // Stop the part.
      rb.velocity = Vector3.zero;
      rb.angularVelocity = Vector3.zero;
      rb.constraints = RigidbodyConstraints.FreezeAll;
      rb.Sleep();
   }

   public void Select()
   {
      this.GetComponent<Mover>().enabled = true;
      this.GetComponent<Rigidbody>().useGravity = false;
   }

   public void Unselect()
   {
      this.GetComponent<Mover>().enabled = false;
   }
*/
