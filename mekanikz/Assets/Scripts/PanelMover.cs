﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelMover : MonoBehaviour
{
    private enum Mode {
        Idle,
        Opening,
        Closing
    }

    private enum State {
        Opened,
        Closed,
        Sliding
    }
    private GameObject leftPanel;
    private GameObject rightPanel;

    public GameObject stageFloor;

    private Vector3 closedPositionLP;
    private Vector3 closedPositionRP;

    private Vector3 openedPositionLP;
    private Vector3 openedPositionRP;

    private Mode mode = Mode.Idle;

    private float t;

    public float slideDuration;

    private State state = State.Closed;

    // Start is called before the first frame update
    void Start()
    {
        stageFloor = GameObject.Find("floor");
        leftPanel = transform.GetChild(0).gameObject;
        rightPanel = transform.GetChild(1).gameObject;

        closedPositionLP = leftPanel.transform.position;
        closedPositionRP = rightPanel.transform.position;

        openedPositionLP = new Vector3(closedPositionLP.x-stageFloor.transform.localScale.x/2,
                                       closedPositionLP.y,
                                       closedPositionLP.z);

        openedPositionRP = new Vector3(closedPositionRP.x+stageFloor.transform.localScale.x/2,
                                       closedPositionRP.y,
                                       closedPositionRP.z);
        Open();
    }

    // Update is called once per frame
    void Update()
    {
        switch(mode){
            case Mode.Opening:
                t += Time.deltaTime/slideDuration;
                leftPanel.transform.position = Vector3.Lerp(closedPositionLP, openedPositionLP, t);
                rightPanel.transform.position = Vector3.Lerp(closedPositionRP,openedPositionRP, t);
                if(leftPanel.transform.position == openedPositionLP) {
                    state = State.Opened;
                    mode = Mode.Idle;
                }
                break;
            case Mode.Closing:
                t += Time.deltaTime/slideDuration;
                leftPanel.transform.position = Vector3.Lerp(openedPositionLP, closedPositionLP, t);
                rightPanel.transform.position = Vector3.Lerp(openedPositionRP, closedPositionRP, t);
                if(leftPanel.transform.position == closedPositionLP) {
                    state = State.Closed;
                    mode = Mode.Idle;
                }
                break;
            default: break;
        }
    }

    public void Open()
    {
        t = 0.0f;
        mode = Mode.Opening;
        state = State.Sliding;
    }
    public void Close()
    {
        t = 0.0f;
        mode = Mode.Closing;
        state = State.Sliding;
    }

    public void Toggle()
    {
        switch(state){
            case State.Closed: Open(); break;
            case State.Opened: Close(); break;
            default: break;
        }
    }
}


/*
float t;
    Vector3 startPosition;
    Vector3 target;
    float timeToReachTarget;
     void Start()
     {
             startPosition = target = transform.position;
     }
     void Update() 
     {
             t += Time.deltaTime/timeToReachTarget;
             transform.position = Vector3.Lerp(startPosition, target, t);
     }
     public void SetDestination(Vector3 destination, float time)
     {
            t = 0;
            startPosition = transform.position;
            timeToReachTarget = time;
            target = destination; 
     }
*/