﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Piece
{
   public GameObject pieceGameObject;
   public Vector3 startPosition { get; private set; }

   Piece (GameObject pieceGameObject)
   {
      this.pieceGameObject = pieceGameObject;
      startPosition = pieceGameObject.transform.position;
   }

   public void SaveAsStartPosition()
   {
      startPosition = pieceGameObject.transform.position;
   }

   public void ResetPosition()
   {
      pieceGameObject.transform.position = startPosition;
   }
}

public class PieceManager : MonoBehaviour
{
   private GameObject stageFloor;
   private float stageWidth;

   public float maxDistSq = 0.1f;
   public List<Piece> pieces;

/*
    public float explosionForce = 15.0f;
   [Range(1.0f,2.0f)]
   public float explosionForceVariabilityFactor = 2.0f;
   public float explosionRadius = 10.0f;

   [Range(1.0f,2.0f)]
   public float explosionRadiusVariabilityFactor = 2.0f;
   [Range(1.0f,10.0f)]
   public float explosionImpactOffsetVariability = 5.0f;
 */
   void Awake() {
      string text = "";

      stageFloor = GameObject.Find("floor");
      stageWidth = stageFloor.GetComponent<Collider>().bounds.size.x;

      foreach(Piece piece in pieces) {
         piece.SaveAsStartPosition();
         text += "\n\rPiece info("+piece.pieceGameObject.name+", "+piece.startPosition+")";
      }
      Debug.Log(text);
   }

   public void BreakApart()
   {

      foreach (Piece piece in pieces)
      {
         float newX = Mathf.Clamp(Random.Range(-stageWidth/2, stageWidth/2) + piece.startPosition.x, -stageWidth/2, stageWidth/2) + stageFloor.transform.position.x;
         float newY = stageFloor.transform.position.y + 3.0f;

         piece.pieceGameObject.transform.position = new Vector3(newX, newY, piece.startPosition.z);
         piece.pieceGameObject.GetComponent<Rigidbody>().useGravity = true;
         piece.pieceGameObject.GetComponent<Rigidbody>().isKinematic = true;
      }
   }

   public Score GetScore()
   {
      int correctPositionCount = 0;

      foreach(Piece piece in pieces)
         correctPositionCount += (piece.pieceGameObject.transform.position - piece.startPosition).sqrMagnitude < maxDistSq ? 1 : 0;

      return new Score(pieces.Count, correctPositionCount);
   }

   public void ResetPieces(){
      foreach(Piece piece in pieces)
         piece.pieceGameObject.transform.position = piece.startPosition;
   }

/*
   public GameObject GetClosestPart(GameObject part)
   {
      GameObject closestGameObject = null;
      float distSq = Mathf.Infinity;

      foreach (Piece piece in pieces)
      {
         GameObject curPart = curPartInfo.part;

         if (part != curPart)
         {
            Vector3 dist = part.transform.position - curPart.transform.position;
            float curDistSq = dist.sqrMagnitude;
            if (curDistSq < distSq)
            {
               distSq = curDistSq;
               closestGameObject = curPart;
            }
         }
      }

      return closestGameObject;
   }
*/
}
