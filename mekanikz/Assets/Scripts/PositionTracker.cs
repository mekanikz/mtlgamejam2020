﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionTracker : MonoBehaviour
{
    public int MAX_SCORE = 5;
    public int MAX_DISTANCE = 20;

    public GameObject stageFloorGameObject;

    private class PieceCoords {
        public GameObject go;
        public float origX;
        public float origY;

        public float finalX;

        public float finalY;

        public float distX;

        public float distY;

        public PieceCoords(GameObject obj, float x, float y){
            this.go = obj;
            this.origX = x;
            this.origY = y;
        }
    }

    private List<PieceCoords> pieceCoords;

    public bool pieceLock = false;

    public float defaultThrust = 1.0f;

    public GameObject selectedMovePiece;

    // Start is called before the first frame update
    public void Awake()
    {
        pieceCoords = new List<PieceCoords>();
        pieceLock = false;

        //Debug.Log("Number of child objects: " + this.transform.childCount.ToString());

        for (int i = 0; i < transform.childCount; i++){
            pieceCoords.Add(new PieceCoords(transform.GetChild(i).gameObject,
                                            transform.GetChild(i).transform.position.x,
                                            transform.GetChild(i).transform.position.y));
        }

        BreakIt();
    }

    public void FreezeFinalCoords()
    {
        foreach(PieceCoords item in pieceCoords){
            item.finalX = item.go.transform.position.x;
            item.finalY = item.go.transform.position.y;
            item.distX = Mathf.Abs(item.finalX-item.origX);
            item.distY = Mathf.Abs(item.finalY-item.origY);
        }
    }

    private float getPieceScore(PieceCoords piece)
    {
        float distFromOrig = 
            Mathf.Max(Mathf.Abs(piece.finalX-piece.origX) + Mathf.Abs(piece.finalY-piece.origY),
                      MAX_DISTANCE);

        return MAX_SCORE - distFromOrig / MAX_SCORE;
    }

    public int GetArtScore()
    {
        float score = 0.0f;

        foreach(PieceCoords item in pieceCoords){
            score += getPieceScore(item);
        }

        return Mathf.RoundToInt(score / pieceCoords.Count);
    }

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BreakIt()
    {
        if(stageFloorGameObject == null) {
            return;
        }

        float stageX = stageFloorGameObject.transform.position.x;
        float stageY = stageFloorGameObject.transform.position.y;
        float stageWidth = stageFloorGameObject.transform.localScale.x;

        foreach(PieceCoords piece in pieceCoords){
            float newX = Mathf.Clamp(Random.Range(-10.0f,10.0f)+piece.origX,-10.0f,10.0f)+stageFloorGameObject.transform.position.x;
            float newY = stageFloorGameObject.transform.position.y + 3.0f;

            piece.go.transform.position = new Vector3(newX,newY);
            piece.go.GetComponent<Rigidbody>().useGravity = true;
        }  
    }

}
