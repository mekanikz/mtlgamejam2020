﻿public class Score {
   public int pieceCount { get; private set;}
   public int placedPieceCount { get; private set; }

   public Score(int pieceCount, int placedPieceCount) {
       this.pieceCount = pieceCount;
       this.placedPieceCount = placedPieceCount;
   }
}
