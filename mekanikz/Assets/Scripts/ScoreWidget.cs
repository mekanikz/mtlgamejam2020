﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreWidget : MonoBehaviour
{
   public void UpdateScoreText(Score score)
   {
      string scoreInfo = "Score: "+score.placedPieceCount.ToString()+"/"+score.pieceCount.ToString();
      Debug.Log(scoreInfo);

      GetComponent<Text>().text = scoreInfo;
   }
}
