﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPart : MonoBehaviour
{
   public bool IsSelected { get; set; }

   // Start is called before the first frame update
   void Start()
   {
      IsSelected = false;
   }
}
