﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float thrust = 100f;
    Rigidbody rb;
    Select sel;

   // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        sel = GetComponent<Select>();
        
        StopPart();
    }

    void FixedUpdate()
    {
        if (sel.isSelected)
        {
            if (Input.GetKey(KeyCode.W))
            {
                AddForce(transform.up);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                AddForce(-transform.right);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                AddForce(-transform.up);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                AddForce(transform.right);
            }
        } 
    }

    void OnCollisionEnter(Collision collision)
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.constraints = RigidbodyConstraints.FreezeAll;
    }

    void AddForce(Vector3 direction)
    {
        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        rb.WakeUp();
        rb.AddForce(direction * thrust);
    }

    void StopPart()
    {
        // Stop the part.
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.constraints = RigidbodyConstraints.FreezeAll;
        rb.Sleep();
    }
}