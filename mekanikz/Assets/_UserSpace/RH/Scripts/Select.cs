﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Select : MonoBehaviour
{
    public bool isSelected = false;
    public string selectableTag = "Selectable";
    private Transform _selection;

    // Update is called once per frame
    void Update()
    {
        if (_selection != null)
        {
            var selectionRenderer = _selection.GetComponent<Renderer>();
            //selectionRenderer.material = defaultMaterial; This code can be used to update the texture back to a it's texture
            _selection = null;
        }

        if (Input.GetButtonUp("Fire1")) //When the fire 1 key is realeased.....
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition); //the raycast begins...
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) 
            {
                var selection = hit.transform;
                if (selection.CompareTag(selectableTag)) //If it hits a selectable object
                {
                    var selectionRenderer = selection.GetComponent<Renderer>(); 
                    if (selectionRenderer != null)
                    {
                        //selectionRenderer.material = highlightMaterial; This code can be used to update the texture to a "selected texture"
                        print("Succeesss");
                        isSelected = true; //is selected becomes true and the object can move. (See Move.cs)                       
                    }

                    _selection = selection;
                }
            }
        }
    }
}
